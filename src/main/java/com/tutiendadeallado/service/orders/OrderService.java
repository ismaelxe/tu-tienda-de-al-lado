package com.tutiendadeallado.service.orders;

import com.tutiendadeallado.dao.orders.OrderDao;
import com.tutiendadeallado.domain.orders.Order;
import com.tutiendadeallado.service.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrderService extends AbstractService<Order> {

    @Autowired
    private OrderDao orderDao;

    @Override
    @Transactional(readOnly = true)
    public Order findById(Long id) {
        return orderDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> findAll() {
        return (List<Order>) orderDao.findAll();
    }


    @Transactional(readOnly = true)
    public List<Order> findByAccountId(Long accountId){
        return orderDao.findByAccount_Id(accountId);
    }


    @Override
    public Order save(Order entity) {
        return orderDao.save(entity);
    }

    @Override
    public void delete(Order entity) {
        orderDao.delete(entity);
    }

    @Override
    public void deleteById(Long id) {
        orderDao.deleteById(id);
    }
}
