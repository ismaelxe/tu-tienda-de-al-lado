package com.tutiendadeallado.service;

import com.tutiendadeallado.dao.ShopDao;
import com.tutiendadeallado.domain.Account;
import com.tutiendadeallado.domain.Shop;
import com.tutiendadeallado.dto.ShopDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Service
public class ShopService extends AbstractService<Shop> {

    @Autowired
    private ShopDao shopDao;

    @Override
    public Shop findById(Long id) {
        return shopDao.findById(id).orElse(null);
    }

    @Override
    public List<Shop> findAll() {
        return (List<Shop>) shopDao.findAll();
    }

    @Override
    public void delete(Shop entity) {
        shopDao.delete(entity);
    }

    @Override
    public void deleteById(Long id) {
        shopDao.deleteById(id);
    }

    @Override
    public Shop save(Shop shop) {
        return shopDao.save(shop);
    }


    @Transactional(readOnly = true)
    public List<Shop> findByAccountId(Long accountId) {
        return shopDao.findByAccount_Id(accountId);
    }


}
