package com.tutiendadeallado.service;

import com.tutiendadeallado.dao.AccountDao;
import com.tutiendadeallado.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AccountService extends AbstractService<Account>   {

    @Autowired
    private AccountDao accountDao;


    public Account findById(Long id) {
        return accountDao.findById(id).orElse(null);
    }

    public List<Account> findAll() {
        return (List<Account>) accountDao.findAll();
    }

    public Account save(Account account) {
        return accountDao.save(account);
    }

    @Override
    public void delete(Account account) {
        accountDao.delete(account);
    }

    @Override
    public void deleteById(Long id) {
        accountDao.deleteById(id);
    }

    public Account findByEmail(String email) {
        accountDao.findByEmail(email);
        return null;
    }

    public Account findByEmailAndPassword(String email, String password) {
        return accountDao.findByEmailAndPassword(email, password);
    }
}
