package com.tutiendadeallado.service;


import com.tutiendadeallado.domain.File;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService extends IService<File> {

    public File save(MultipartFile multipartFile);

    public byte[] downloadFile(Long fileId);

    }
