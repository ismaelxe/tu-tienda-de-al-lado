package com.tutiendadeallado.service;

import com.tutiendadeallado.dao.FileDao;
import com.tutiendadeallado.domain.File;
import com.tutiendadeallado.property.FileStorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.UUID;

@Service
public class FileStorageServiceImpl extends AbstractService<File>  implements FileStorageService{

    private static Path fileStorageLocation;

    @Autowired
    public FileStorageServiceImpl(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getImageDir())
                .toAbsolutePath().normalize();

//        try {
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        } catch (Exception ex) {
////            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
//
//        }
    }

    @Autowired
    private FileDao fileDao;


    @Override
    @Transactional(readOnly = true)
    public File findById(Long id) {
        return fileDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<File> findAll() {
        return (List<File>) fileDao.findAll();
    }

    @Override
    @Transactional
    public File save(File entity) {
        return fileDao.save(entity);


    }


    @Transactional
    public File save(MultipartFile multipartFile) {
        File file = null;

        try {
            file = new File();
            file.setOriginalName(multipartFile.getOriginalFilename());
            file.setUniqueFilename(UUID.randomUUID().toString() + "_" + multipartFile.getOriginalFilename());
            file.setContentType(multipartFile.getContentType());

            file = fileDao.save(file);

            Path targetLocation = fileStorageLocation.resolve(file.getUniqueFilename());

            Files.copy(multipartFile.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);


        } catch (IOException e) {
            e.printStackTrace();
        }


        return file;

    }

    @Override
    @Transactional(readOnly = true)
    public byte[] downloadFile(Long fileId) {
        InputStream is = null;

        try {
            File file = fileDao.findById(fileId).orElseThrow();
            Path filePath = this.fileStorageLocation.resolve(file.getUniqueFilename()).normalize();
             is = new FileInputStream(filePath.toFile());


        return is.readAllBytes();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }


    @Override
    @Transactional
    public void delete(File entity) {
        fileDao.delete(entity);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        fileDao.deleteById(id);
    }
}
