package com.tutiendadeallado.service;

import com.tutiendadeallado.domain.Shop;
import com.tutiendadeallado.dto.ShopDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IService<T> {

    @Transactional(readOnly = true)
    public T findById(Long id);

    @Transactional(readOnly = true)
    public List<T> findAll();


    public T save(T entity);

    public void delete(T entity);

    public void deleteById(Long id);

}
