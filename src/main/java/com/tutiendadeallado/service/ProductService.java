package com.tutiendadeallado.service;

import com.tutiendadeallado.dao.ProductDao;
import com.tutiendadeallado.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService extends AbstractService<Product> {

    @Autowired
    private ProductDao productDao;


    @Override
    @Transactional(readOnly = true)
    public Product findById(Long id) {
        return productDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Product> findAll() {
        List<Product> products = new ArrayList<>();
        productDao.findAll().forEach(products::add);
        return products;
    }

    @Transactional
    public List<Product> findByAccountId(Long id){
        return productDao.findByShop_Account_Id(id);
    }


    @Transactional
    public List<Product> findByShopId(Long id){
        return productDao.findByShopId(id);
    }

    @Override
    @Transactional
    public Product save(Product entity) {
        return productDao.save(entity);
    }

    @Override
    @Transactional
    public void delete(Product entity) {
        productDao.delete(entity);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        productDao.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<Product> findRandom() {
        return productDao.findFirst5ByOrderByIdDesc();
    }
}
