package com.tutiendadeallado.service;

import com.tutiendadeallado.dao.RoleDao;
import com.tutiendadeallado.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RoleService extends AbstractService<Role> {

    @Autowired
    private RoleDao roleDao;


    @Transactional(readOnly = true)
    public List<Role> findByAccountId(Long accountId){
        return roleDao.findByAccountId(accountId);
    }

    @Override
    @Transactional(readOnly = true)
    public Role findById(Long id) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Role> findAll() {
        return null;
    }

    @Override
    @Transactional
    public Role save(Role entity) {
        return roleDao.save(entity);
    }

    @Override
    @Transactional
    public void delete(Role entity) {

    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        roleDao.deleteById(id);
    }
}
