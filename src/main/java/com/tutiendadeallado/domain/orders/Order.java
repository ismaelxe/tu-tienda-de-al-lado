package com.tutiendadeallado.domain.orders;

import com.tutiendadeallado.domain.AbstractEntityCreateAt;
import com.tutiendadeallado.domain.Account;

import javax.persistence.*;
import java.util.List;

@Entity(name = "orders")
public class Order extends AbstractEntityCreateAt {


    @Column
    private Boolean paid;
    @Column
    private Boolean deleted;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
    private List<OrderDetail> orderDetails;


    @PrePersist
    private  void prePersist(){
        Boolean paid = Boolean.FALSE;
        Boolean deleted = Boolean.FALSE;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }
}
