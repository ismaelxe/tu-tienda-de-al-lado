package com.tutiendadeallado.domain.orders;

import com.tutiendadeallado.domain.AbstractEntity;
import com.tutiendadeallado.domain.Product;

import javax.persistence.*;
import java.util.Date;

@Entity
public class OrderDetail extends AbstractEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private Order order;

    @Column
    private Double price;

    @Column
    private Integer quantity;

    @Column
    private Double total;

    @Column(name = "ship_date")
    @Temporal(TemporalType.DATE)
    private Date shipDate;

    //TODO Descuentos
//    private Discount discount;


    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }
}
