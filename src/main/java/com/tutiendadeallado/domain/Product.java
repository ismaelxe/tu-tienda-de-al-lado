package com.tutiendadeallado.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Product extends AbstractEntityCreateAt {

    @Column
    private String name;
    @Column
    private String description;

    @Column(name = "principal_image_id")
    private Long principalImage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shop_id")
    private Shop shop;

    @Column(name = "price")
    private Double price;

    @Column(name = "deleted")
    private Boolean deleted = Boolean.FALSE;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getPrincipalImage() {
        return principalImage;
    }

    public void setPrincipalImage(Long principalImage) {
        this.principalImage = principalImage;
    }
}
