package com.tutiendadeallado.domain;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class File extends AbstractEntityCreateAt {
    @Column(name = "original_name")
    private String originalName;

    @Column(name = "unique_filename")
    private String uniqueFilename;

    @Column(name = "media_type")
    private String contentType;


    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getUniqueFilename() {
        return uniqueFilename;
    }

    public void setUniqueFilename(String uniqueFilename) {
        this.uniqueFilename = uniqueFilename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }




}
