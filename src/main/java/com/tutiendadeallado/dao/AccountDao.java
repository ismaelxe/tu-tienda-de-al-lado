package com.tutiendadeallado.dao;

import com.tutiendadeallado.domain.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountDao extends CrudRepository<Account, Long> {
    public Account findByEmail(String email);

    public Account findByEmailAndPassword(String email, String pass);
}
