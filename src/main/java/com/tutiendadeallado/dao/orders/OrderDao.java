package com.tutiendadeallado.dao.orders;

import com.tutiendadeallado.domain.orders.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderDao extends CrudRepository<Order, Long> {

    public List<Order> findByAccount_Id(Long accountId);

}
