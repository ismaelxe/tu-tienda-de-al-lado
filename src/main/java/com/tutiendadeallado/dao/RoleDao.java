package com.tutiendadeallado.dao;

import com.tutiendadeallado.domain.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoleDao extends CrudRepository<Role, Long> {


    public List<Role> findByAccountId(Long accountId);


}
