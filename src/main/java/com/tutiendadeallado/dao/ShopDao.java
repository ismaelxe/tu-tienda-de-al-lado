package com.tutiendadeallado.dao;

import com.tutiendadeallado.domain.Shop;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ShopDao extends CrudRepository<Shop, Long> {

    public List<Shop> findByAccount_Id(Long id);


}
