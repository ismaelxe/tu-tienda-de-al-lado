package com.tutiendadeallado.dao;

import com.tutiendadeallado.domain.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductDao extends CrudRepository<Product, Long> {

    public List<Product> findByShopId(Long shopId);

    public List<Product> findByShop_Account_Id(Long accountId);

    public List<Product> findFirst5ByOrderByIdDesc();


}
