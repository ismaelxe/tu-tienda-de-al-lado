package com.tutiendadeallado.dao;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import java.io.Serializable;

@Scope("prototype")
@Repository
public abstract class AbstractDao<T> implements Serializable, org.springframework.data.repository.CrudRepository<T, Long> {


}
