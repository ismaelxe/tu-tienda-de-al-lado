package com.tutiendadeallado.utils;

import com.tutiendadeallado.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordUtils {





    public static void encodePassword(Account account){
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String textPlainPassword = account.getPassword();
        account.setPassword(passwordEncoder.encode(textPlainPassword));
    }


}
