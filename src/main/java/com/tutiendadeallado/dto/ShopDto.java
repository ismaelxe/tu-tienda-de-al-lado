package com.tutiendadeallado.dto;

import com.tutiendadeallado.domain.Account;
import com.tutiendadeallado.domain.Shop;

public class ShopDto {

    private Long id;
    private String name;
    private String accountId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
