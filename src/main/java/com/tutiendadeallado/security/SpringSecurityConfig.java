package com.tutiendadeallado.security;

import com.tutiendadeallado.auth.filter.JWTAuthenticationFilter;
import com.tutiendadeallado.auth.filter.JWTAuthorizationFilter;
import com.tutiendadeallado.auth.service.JWTService;
import com.tutiendadeallado.service.JPAUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private JPAUserDetailsService userDetailsService;

    @Autowired
    private JWTService jwtService;

    public SpringSecurityConfig(JPAUserDetailsService userDetailsService){
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/**").permitAll()

                //SuperAdmin
                .antMatchers("/account/**").authenticated()
//                .antMatchers("/account/**").hasAnyAuthority("SUPERADMIN")
                ///////////

                .anyRequest().authenticated()

                .and()
                .addFilter(new JWTAuthenticationFilter(authenticationManager(), jwtService))
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), jwtService))
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    }

    @Autowired
    public void configurerGlobal(AuthenticationManagerBuilder build) throws Exception
    {

        build.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder);
    }
}
