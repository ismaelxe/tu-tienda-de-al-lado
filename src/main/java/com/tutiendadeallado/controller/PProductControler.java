package com.tutiendadeallado.controller;

import com.tutiendadeallado.domain.Product;
import com.tutiendadeallado.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PProductControler extends AbstractController{

    @Autowired
    private ProductService productService;


    @GetMapping("random-products")
    public List<Product> findRandomProducts(){
        return  productService.findRandom();
    }

    @GetMapping("product/{productId}")
    public Product findById(@PathVariable("productId") Long productId){
        return  productService.findById(productId);
    }


}
