package com.tutiendadeallado.controller;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

//@CrossOrigin(origins="*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE, RequestMethod.OPTIONS })
public abstract class AbstractController<T> {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    private T t;
    protected static ModelMapper modelMapper = new ModelMapper();





    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String error(){
        return "Error desconocido";
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String AccessDeniedException(){
        return "Permission Denied";
    }


    public <T> T map(Object source) {
        return (T) modelMapper.map(source, t.getClass());
    }



}
