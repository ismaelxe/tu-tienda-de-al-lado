package com.tutiendadeallado.controller.orders;

import com.tutiendadeallado.controller.AbstractController;
import com.tutiendadeallado.domain.orders.Order;
import com.tutiendadeallado.service.orders.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/account/{accountId}/order")
@PreAuthorize("hasAnyRole('CLIENT')")
public class OrderController extends AbstractController {

    @Autowired
    private OrderService orderService;

    @GetMapping
    public List<Order> findByAccountId(@PathVariable("accountId") Long accountId){
        return orderService.findByAccountId(accountId);
    }

    @GetMapping("/orderId")
    public Order findOrderById(@PathVariable("orderId") Long orderId){
        return orderService.findById(orderId);
    }

    @PostMapping()
    public Order createOrder(@RequestBody Order order){
        return orderService.save(order);
    }

    @PutMapping()
    public Order updateOrder(@RequestBody Order order){
        //TODO order update
        return orderService.save(order);
    }

    @DeleteMapping("/orderId")
    public void deleteOrder(@PathVariable Long orderId){
        orderService.deleteById(orderId);
    }

}
