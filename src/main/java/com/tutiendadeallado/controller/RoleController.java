package com.tutiendadeallado.controller;

import com.tutiendadeallado.domain.AuthorityType;
import com.tutiendadeallado.domain.Role;
import com.tutiendadeallado.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@PreAuthorize("hasRole('SUPERADMIN')")
public class RoleController extends AbstractController {

    @Autowired
    private RoleService roleService;


    @GetMapping(value = "/accounts/{accountId}/roles/")
    public List<Role> findRolesByAccountId(@NotNull @PathVariable("accountId") Long accountId){
        List<Role> roles = roleService.findByAccountId(accountId);
        //TODO crear DTO
        return roles;
    }


    @PostMapping("roles/")
    @ResponseStatus(value = HttpStatus.CREATED)
    public Role createRole(@RequestBody Role role){
            AuthorityType.valueOf(role.getAuthority());  //Si no esta en la enumeracion provocará una excepcion
            return roleService.save(role);
    }



    @DeleteMapping("roles/{roleId}")
    public void deleteRoleById(@PathVariable("roleId") Long roleId){
        roleService.deleteById(roleId);
    }


    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String badRequest(Exception e){
        return "Bad Request: " + e;
    }

}
