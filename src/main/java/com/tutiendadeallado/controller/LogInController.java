package com.tutiendadeallado.controller;

import com.tutiendadeallado.domain.Account;
import com.tutiendadeallado.dto.AccountDto;
import com.tutiendadeallado.service.AccountService;
import com.tutiendadeallado.utils.PasswordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class LogInController extends AbstractController {

    @Autowired
    private AccountService accountService;



    @PreAuthorize("isAuthenticated()")
    @PostMapping(value = "/logout")
    public void logout(@RequestBody AccountDto accountDto){
        //TODO logout
    }


    @PostMapping(value = "/sign-in", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Account save (@RequestBody Account account){
        PasswordUtils.encodePassword(account);
        accountService.save(account);

        return account;
    }



}
