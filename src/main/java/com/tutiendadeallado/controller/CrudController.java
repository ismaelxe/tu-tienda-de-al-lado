package com.tutiendadeallado.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;


public interface CrudController<T> extends Serializable {

    @GetMapping(value = "")
    public List<T> list();

    @GetMapping(value = "/{id}")
    public T view(@PathVariable("id") Long id);

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "")
    public T save(@RequestBody T entity);


    @ResponseStatus(HttpStatus.CREATED)
    @PutMapping(value = "/{id}")
    public T update(@RequestBody T entity, @PathVariable("id") Long id);

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable("id") Long id);


}
