package com.tutiendadeallado.controller.account;

import com.tutiendadeallado.controller.AbstractController;
import com.tutiendadeallado.domain.Product;
import com.tutiendadeallado.domain.Shop;
import com.tutiendadeallado.dto.ProductDto;
import com.tutiendadeallado.service.ProductService;
import com.tutiendadeallado.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "account/{acccountId}/shop/{shopId}/product")
@PreAuthorize("hasAnyRole('VENDOR')")
public class ProductController extends AbstractController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ShopService shopService;


    @GetMapping("")
    public ProductDto[] findByShop(@PathVariable("shopId") Long shopId){
        List<Product> products = productService.findByShopId(shopId);
        return modelMapper.map(products, ProductDto[].class);
    }

    @GetMapping("/{productId}")
    public ProductDto findById(@PathVariable("productId") Long productId){
        Product product = productService.findById(productId);

        return modelMapper.map(product, ProductDto.class);
    }

    @PostMapping("")
    public ProductDto create(@PathVariable("shopId") Long shopId, @RequestBody Product product){
        Shop shop = shopService.findById(shopId);
        product.setShop(shop);
        Product productCreated = productService.save(product);
        return modelMapper.map(productCreated, ProductDto.class);
    }

    @PutMapping("")
    public ProductDto update(Product product){
        Product productCreated = productService.save(product);
        return modelMapper.map(productCreated, ProductDto.class);
    }

    @DeleteMapping("/{productId}")
    public void deleteById(@PathVariable Long productId){
        productService.deleteById(productId);
    }



}
