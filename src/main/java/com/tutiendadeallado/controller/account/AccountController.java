package com.tutiendadeallado.controller.account;

import com.tutiendadeallado.controller.AbstractController;
import com.tutiendadeallado.domain.Account;
import com.tutiendadeallado.dto.AccountDto;
import com.tutiendadeallado.service.AccountService;
import com.tutiendadeallado.utils.PasswordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountController extends AbstractController {

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private AccountService accountService;



    @PreAuthorize("hasRole('SUPERADMIN')")
    @GetMapping(value = "",produces = MediaType.APPLICATION_JSON_VALUE)
    public AccountDto[] list() {
        List<Account> accounts = accountService.findAll();
        List<AccountDto> accountDtos = new ArrayList<>();

        return modelMapper.map(accounts, AccountDto[].class);
    }



    @PreAuthorize("hasRole('CLIENT')")
    @GetMapping(value = "/{accountId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public AccountDto view(@PathVariable("accountId") Long accountId){
        Account account = accountService.findById(accountId);
        return modelMapper.map(account, AccountDto.class);
    }




    @PreAuthorize("hasAnyRole('SUPERADMIN')")
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Account save (@RequestBody Account account){
        PasswordUtils.encodePassword(account);
        accountService.save(account);

        return account;
    }



    @PreAuthorize("hasRole('CLIENT')")
    @PutMapping(value = "/{accountId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AccountDto update (@RequestBody Account account, @PathVariable Long accountId){
        Account originalAccount = accountService.findById(accountId);
        if(originalAccount != null){
            originalAccount.setName(account.getName());
            originalAccount.setEmail(account.getEmail());
            originalAccount.setPassword(account.getPassword());
            PasswordUtils.encodePassword(originalAccount);
            accountService.save(originalAccount);
        }

        return modelMapper.map(originalAccount, AccountDto.class);
    }



    @PreAuthorize("hasRole('CLIENT')")
    @DeleteMapping(value = "/{accountId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete (@PathVariable Long accountId){
        Account account = accountService.findById(accountId);
        accountService.delete(account);
    }


}
