package com.tutiendadeallado.controller.account;

import com.tutiendadeallado.controller.AbstractController;
import com.tutiendadeallado.domain.Account;
import com.tutiendadeallado.domain.Shop;
import com.tutiendadeallado.dto.ShopDto;
import com.tutiendadeallado.service.AccountService;
import com.tutiendadeallado.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "account/{acccountId}/shop")
public class ShopController extends AbstractController {

    @Autowired
    private ShopService shopService;

    @Autowired
    private AccountService accountService;

    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('VENDOR')")
    public List<ShopDto> list(@PathVariable("acccountId") Long accountId) {
        List<Shop> shops = shopService.findByAccountId(accountId);

        List<ShopDto> shopDtos = new ArrayList<>();

        shops.forEach(shop -> {
            ShopDto shopDto = modelMapper.map(shop, ShopDto.class);

            shopDtos.add(shopDto);
        });
        return shopDtos;
    }

    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAnyRole({'VENDOR', 'CLIENT'})")
    public ShopDto view(@PathVariable("id") Long id) {
        Shop shop = shopService.findById(id);
        return modelMapper.map(shop, ShopDto.class);
    }


    @PreAuthorize("hasAnyRole({'VENDOR'})")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "")
    public ShopDto save(@PathVariable("acccountId") Long accountId, @RequestBody Shop entity) {
        Account account = accountService.findById(accountId);
        ShopDto shopDto;
        entity.setAccount(account);
        shopDto = modelMapper.map(shopService.save(entity), ShopDto.class);
        return shopDto;
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyRole({'VENDOR'})")
    @PutMapping(value = "/{id}")
    public ShopDto update(@RequestBody Shop entity, @PathVariable("id") Long id) {
        ShopDto shopDto;
        Shop shop = shopService.findById(id);
        shop.setName(entity.getName());
        shopDto = modelMapper.map(shopService.save(shop), ShopDto.class);
        return shopDto;
    }



    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole({'VENDOR'})")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable("id")  Long id) {
        shopService.deleteById(id);
    }
}
