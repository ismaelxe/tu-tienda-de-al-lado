package com.tutiendadeallado.controller;

import com.tutiendadeallado.domain.File;
import com.tutiendadeallado.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

@RestController
@RequestMapping("file")
public class FileController extends AbstractController {

    @Autowired
    private FileStorageService fileService;

    @GetMapping()
    public List<File> getAll(){
        return fileService.findAll();
    }


    @GetMapping("{fileId}/download")
    public byte[] downloadFile(@PathVariable("fileId") Long fileId){
        byte[] fileBytes = fileService.downloadFile(fileId);
        return fileBytes;
    }


        @GetMapping("{fileId}/image64")
    public String downloadImagebase64(@PathVariable("fileId") Long fileId){
        byte[] fileBytes = fileService.downloadFile(fileId);
        return new String(Base64.getEncoder().encode(fileBytes), StandardCharsets.UTF_8);

    }


    @PostMapping()
    public File uploadFile(@RequestParam("file") MultipartFile multipartFile){
        File file = fileService.save(multipartFile);
        return file;
    }






}
